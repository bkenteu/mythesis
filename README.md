# myThesis

[Details of the Requirements for the MSc Dissertations from lucalong.eu](https://lucalongo.eu/MScDissertationsTUD/)

# Pipeline


[![pipeline status](https://gitlab.com/dogmountain/mythesis/badges/master/pipeline.svg)](https://gitlab.com/dogmountain/mythesis/-/commits/master)


# Progress


### Chapter 1 – Introduction ![](https://us-central1-progress-markdown.cloudfunctions.net/progress/100)


### Chapters 2 – Literature review and related work ![](https://us-central1-progress-markdown.cloudfunctions.net/progress/100)


### Chapter 3 – Design and methodology ![](https://us-central1-progress-markdown.cloudfunctions.net/progress/100)


### Chapter 4 – Results, evaluation and discussion ![](https://us-central1-progress-markdown.cloudfunctions.net/progress/100)


### Chapter 5. Conclusion ![](https://us-central1-progress-markdown.cloudfunctions.net/progress/100)


