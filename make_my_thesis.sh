#!/bin/bash
notify-send make_my_thesis.sh started
rm -f /home/kent/college/Thesis/myThesis/skeleton.aux
rm -f /home/kent/college/Thesis/myThesis/skeleton.bcf
rm -f /home/kent/college/Thesis/myThesis/skeleton.out
rm -f /home/kent/college/Thesis/myThesis/skeleton.lot
rm -f /home/kent/college/Thesis/myThesis/skeleton.lof
rm -f /home/kent/college/Thesis/myThesis/skeleton.blg
rm -f /home/kent/college/Thesis/myThesis/skeleton.toc
docker run --rm --name mytex \
-v /home/kent/college/Thesis/myThesis:/myfiles \
-w /myfiles registry.gitlab.com/islandoftex/images/texlive \
bash -c "pdflatex skeleton.tex; biber skeleton; pdflatex skeleton.tex;"
return_code=$?
if [ $return_code == "0" ]; then
    exitstr="finished"
else
    exitstr="ERROR! $return_code"
fi
notify-send make_my_thesis.sh "$exitstr"